//
//  UnitsTableViewCell.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 03/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import UIKit

class UnitsTableViewCell: UITableViewCell {

    // MARK: -  Outlets
    @IBOutlet weak var unitsLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        segmentedControl.selectedSegmentIndex = CurrentUser.sharedInstance.unitsSetting ?? 0
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: -  Actions
    @IBAction func segmentedControlButtonAction(sender: UISegmentedControl) {
        CurrentUser.sharedInstance.unitsSetting = sender.selectedSegmentIndex
        guard sender.selectedSegmentIndex == UnitsSegmentedControl.Imperial.rawValue else {
            unitsLabel.text = UnitsDescriptionKey.Metric
            return
        }
        unitsLabel.text = UnitsDescriptionKey.Imperial
    }
}
