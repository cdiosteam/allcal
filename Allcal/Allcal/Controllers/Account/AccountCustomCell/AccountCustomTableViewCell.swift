//
//  AccountCustomTableViewCell.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 04/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import UIKit

class AccountCustomTableViewCell: UITableViewCell {

    // MARK: -  Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    
    // MARK: -  Variables
    var cellType: String?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // asdsadadConfigure the view for the selected state
    }
    
    func setupData(cellType: String?) {
        self.cellType = cellType
        guard cellType == AccountCellType.Notification else {
            self.titleLabel.text = "Vibration"
            self.descriptionLabel.text = "Phone doesn't vibrate on tapping."
            self.switchButton.on = CurrentUser.sharedInstance.vibrationSetting ?? false
            return
        }
        self.titleLabel.text = "Send notifications"
        self.descriptionLabel.text = "Users are not notified of changes you make."
        self.switchButton.on = CurrentUser.sharedInstance.notificationSetting ?? false
    }
    
    @IBAction func switchButtonAction(sender: UISwitch) {
        guard cellType == AccountCellType.Notification else {
            CurrentUser.sharedInstance.vibrationSetting = sender.on
            return
        }
        CurrentUser.sharedInstance.notificationSetting = sender.on
    }
}
