//
//  AccountViewController.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 05/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import UIKit
import Kingfisher

class AccountViewController: UIViewController {
    
    // MARK: -  Outlets
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: -  Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupHeader()
        self.setupTableViewDelegates()
        self.registerNib()
        self.setupTableView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: -  Setup Methods
    func setupHeader() {
        profileImageView.roundImageView()
        self.usernameLabel.text = CurrentUser.sharedInstance.username
        if let urlString = CurrentUser.sharedInstance.imageProfileUrl, let url = NSURL(string: urlString) {
            profileImageView.kf_setImageWithURL(url, placeholderImage: UIImage(named: "tabbar-account"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        }
    }
    
    // MARK: -  Actions
    @IBAction func logoutButtonAction(sender: UIButton) {
        CurrentUser.sharedInstance.logoutCurrentUser()
        AppController.setupLoginRegisterRoot()
    }
}

extension AccountViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view data source
    func registerNib() {
        let unitsNib = UINib(nibName: TableViewCell.UnitsTableViewCell, bundle: nil)
        tableView.registerNib(unitsNib, forCellReuseIdentifier: CellIdentifier.UnitsCellIdentifier)
        
        let accountCustomNib = UINib(nibName: TableViewCell.AccountCustomTableViewCell, bundle: nil)
        tableView.registerNib(accountCustomNib, forCellReuseIdentifier: CellIdentifier.AccountCustomIdentifier)
    }
    
    func setupTableViewDelegates() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func setupTableView() {
        tableView.rowHeight = 50
        tableView.estimatedRowHeight = 50
        tableView.separatorColor = UIColor.clearColor()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.UnitsCellIdentifier, forIndexPath: indexPath) as! UnitsTableViewCell
            cell.selectionStyle = .None
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.AccountCustomIdentifier, forIndexPath: indexPath) as! AccountCustomTableViewCell
            cell.setupData(AccountCellType.Notification)
            cell.selectionStyle = .None
            return cell
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.AccountCustomIdentifier, forIndexPath: indexPath) as! AccountCustomTableViewCell
            cell.setupData(AccountCellType.Vibration)
            cell.selectionStyle = .None
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}
