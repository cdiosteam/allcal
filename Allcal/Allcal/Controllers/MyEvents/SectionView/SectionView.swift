//
//  SectionView.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 11/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation
import UIKit

class SectionView: UIView {
    
    // MARK: -  Outlets
    @IBOutlet weak var dayNumberLabel: UILabel!
    @IBOutlet weak var nameDayLabel: UILabel!
    @IBOutlet weak var monthAndYearLabel: UILabel!
    
    
    override func drawRect(rect: CGRect) {
        
    }
    
    // MARK: -  Setup Methods
    func setupData(date: NSDate) {
        nameDayLabel.text = date.dayOfTheWeek()
        monthAndYearLabel.text = Utils().getStringFromDateWithDateFormat(date, dateFormat: "MMM yyyy")
        dayNumberLabel.text = Utils().getStringFromDateWithDateFormat(date, dateFormat: "dd")
    }
}