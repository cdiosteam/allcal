//
//  MyEventsData.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 09/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation


import UIKit

class MyEventsData {
    
    var events: [Event]?
    var sections = [NSDate]()
    var dictionary = [NSDate: [Event]]()
    var users: [User]?
    
    init(eventsArray: [Event]?, usersArray: [User]?) {
        self.events = eventsArray
        self.users = usersArray
        createDictionary()
        sections.sortInPlace({ $0.compare($1) == NSComparisonResult.OrderedAscending })
    }
    
    func createDictionary() {
        guard let events = self.events else {
            return
        }
        for event in events {
            if let startDate = event.startDate, let endDate = event.endDate {
                
                let startOfDay = startDate.startOfDay
                let startOfDayForEndDate = endDate.startOfDay
                
                if startOfDay.isEqualToDate(startOfDayForEndDate) {
                    if startOfDay.compare(NSDate().startOfDay) == NSComparisonResult.OrderedDescending {
                        var eventsArray = self.dictionary[startOfDay]
                        if eventsArray == nil {
                            eventsArray = [event]
                            self.sections.append(startOfDay)
                        } else {
                            eventsArray?.append(event)
                        }
                        self.dictionary[startOfDay] = eventsArray
                    }
                } else {
                    let calendar = NSCalendar.currentCalendar()
                    let fmt = NSDateFormatter()
                    fmt.dateFormat = "dd/MM/yyyy"
                    var dateIterator = startOfDay
                    
                    while dateIterator.compare(endDate) != .OrderedDescending {
                        if dateIterator.compare(NSDate().startOfDay) == NSComparisonResult.OrderedDescending {
                            var eventsArray = self.dictionary[dateIterator.startOfDay]
                            if eventsArray == nil {
                                eventsArray = [event]
                                self.sections.append(dateIterator.startOfDay)
                            } else {
                                eventsArray?.append(event)
                            }
                            self.dictionary[dateIterator.startOfDay] = eventsArray
                        }
                        dateIterator = calendar.dateByAddingUnit(.Day, value: 1, toDate: dateIterator, options: [])!
                    }
                }
            }
        }
    }
    
}

