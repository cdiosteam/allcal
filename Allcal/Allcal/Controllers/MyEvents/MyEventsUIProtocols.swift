//
//  MyEventsUIProtocols.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 09/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation
import UIKit

typealias MyEventsInteractorProtocol = protocol<MyEventsBaseInteractor, MyEventsActionsInteractor>

protocol MyEventsActionsInteractor {
}

protocol MyEventsBaseInteractor {
    weak var userInterface: MyEventsUserInterface? { get set }
    init(Data initialData: MyEventsData)
    
    func registerNib(tableView: UITableView)
    func setupTableView(tableView: UITableView)
    func setupNavigationBar(navigationBar:  UINavigationBar?, navigationItem: UINavigationItem, title: String)
    func numberOfSectionsInTableView(tableView: UITableView) -> Int 
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
}

protocol MyEventsUserInterface: class {
    var interactor: MyEventsInteractorProtocol? { get set }
    
    func presentViewController(viewController: UIViewController, animated: Bool)
    func pushViewController(viewController: UIViewController, animated: Bool)
    func popViewController(animated: Bool)
    func dismissView(animated: Bool)
}