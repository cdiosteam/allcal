//
//  EventTableViewCell.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 09/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    @IBOutlet weak var fullTitleLabel: UILabel!
    @IBOutlet weak var intervalDateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var lineImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        lineImageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        // Configure the view for the selected state
    }
    
    func setupDataMeEventsCell(event: Event?) {
        guard let event = event else {
            return
        }
        self.fullTitleLabel.text = event.title
        self.intervalDateLabel.text = Utils().getEventIntervalTime(event)
        self.locationLabel.text = event.location
        if event.location == nil || event.location == "" {
            self.locationImageView.hidden = true
        }
        lineImageView.tintColor = event.color
    }
    
}
