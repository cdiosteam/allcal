//
//  MyEventsTableViewController.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 03/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import UIKit

class MyEventsTableViewController: UITableViewController {
    
    
    // MARK: -  Variables
    var interactor: MyEventsInteractorProtocol? {
        didSet {
            interactor?.userInterface = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.interactor?.setupNavigationBar(self.navigationController?.navigationBar, navigationItem: navigationItem, title: "My Events")
        self.interactor?.setupTableView(tableView)
        self.interactor?.registerNib(tableView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.interactor?.numberOfSectionsInTableView(tableView) ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.interactor?.tableView(tableView, numberOfRowsInSection: section) ?? 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return self.interactor?.tableView(tableView, cellForRowAtIndexPath: indexPath) ?? UITableViewCell()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.interactor?.tableView(tableView, didSelectRowAtIndexPath: indexPath)
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.interactor?.tableView(tableView, titleForHeaderInSection: section)
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.interactor?.tableView(tableView, viewForHeaderInSection: section)
    }
    
    static func instantiateInstanceViewController(interactor: MyEventsInteractorProtocol) -> MyEventsTableViewController {
        let instance = MyEventsTableViewController()
        instance.interactor = interactor
        return instance
    }
}

extension MyEventsTableViewController: MyEventsUserInterface {
    func presentViewController(viewController: UIViewController, animated: Bool) {
        self.navigationController?.presentViewController(viewController, animated: animated, completion: nil)
    }
    
    func pushViewController(viewController: UIViewController, animated: Bool) {
        self.navigationController?.pushViewController(viewController, animated: animated)
    }
    
    func popViewController(animated: Bool) {
        self.navigationController?.popViewControllerAnimated(animated)
    }
    
    func dismissView(animated: Bool) {
        
    }
}
