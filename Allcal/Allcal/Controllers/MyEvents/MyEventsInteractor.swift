//
//  MyEventsInteractor.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 09/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation
import UIKit

class MyEventsInteractor: NSObject {
    
    let eventsData: MyEventsData
    weak var userInterface: MyEventsUserInterface?
    
    required init(Data initialData: MyEventsData) {
        self.eventsData = initialData
    }
}

extension MyEventsInteractor: MyEventsBaseInteractor {
    
    func setupTableView(tableView: UITableView) {
        tableView.separatorColor = UIColor.clearColor()
        tableView.backgroundColor = UIColor(red:0.86, green:0.88, blue:0.89, alpha:1.0)
        tableView.estimatedRowHeight = 60
    }
    
    func registerNib(tableView: UITableView) {
        let cellNib = UINib(nibName: TableViewCell.EventTableViewCell, bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: CellIdentifier.MyEventsIdentifier)
        
        let nib = UINib(nibName: TableViewCell.SectionView, bundle: nil)
        tableView.registerNib(nib, forHeaderFooterViewReuseIdentifier: "sectionViewIdentifier")
    }
    
    func addNewEventOrCreateCalendarButtonAction() {
        let alertController = UIAlertController(title: nil, message: "Choose what you want to do:", preferredStyle: UIAlertControllerStyle.ActionSheet)
        let createCalendarButton = UIAlertAction(title: "Create Calendar", style: UIAlertActionStyle.Default) { (action) in
            
        }
        let addEventButton = UIAlertAction(title: "Add Event", style: .Default){ (action) in
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        
        alertController.addAction(createCalendarButton)
        alertController.addAction(addEventButton)
        alertController.addAction(cancelAction)
        userInterface?.presentViewController(alertController, animated: true)
        alertController.view.tintColor = UIColor(red:0.55, green:0.78, blue:0.25, alpha:1.0)
    }
    
    func setupNavigationBar(navigationBar:  UINavigationBar?, navigationItem: UINavigationItem, title: String) {
        navigationItem.title = title
        navigationBar?.titleTextAttributes = [NSForegroundColorAttributeName :  UIColor(red:0.23, green:0.30, blue:0.33, alpha:1.0)]
        navigationBar?.barTintColor = UIColor(colorLiteralRed: 0.91, green: 0.95, blue: 0.95, alpha: 1)
        
        let addNewEventOrCreateCalendarButton = UIBarButtonItem(image: UIImage(named: "btn-create-calendar"), style: .Plain, target: self, action: #selector(MyEventsInteractor.addNewEventOrCreateCalendarButtonAction))
        navigationItem.rightBarButtonItem = addNewEventOrCreateCalendarButton
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.eventsData.dictionary.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = self.eventsData.sections[section]
        let eventsArray = self.eventsData.dictionary[key] as [Event]?
        return eventsArray?.count ?? 0
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(self.eventsData.sections)"
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.MyEventsIdentifier) as? EventTableViewCell else {
            return UITableViewCell()
        }
        let key = self.eventsData.sections[indexPath.section]
        let eventsArray = self.eventsData.dictionary[key] as [Event]?
        let event = eventsArray?[indexPath.row]
        cell.setupDataMeEventsCell(event)
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)  {
        let eventDetails = EventDetailsViewController(nibName: "EventDetailsViewController", bundle: nil)
        let key = self.eventsData.sections[indexPath.section]
        let eventsArray = self.eventsData.dictionary[key] as [Event]?
        let event = eventsArray?[indexPath.row]
        eventDetails.event = event
        eventDetails.users = self.eventsData.users
        self.userInterface?.pushViewController(eventDetails, animated: true)
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionNib: NSArray = NSBundle.mainBundle().loadNibNamed("SectionView", owner: self, options: nil)
        guard let sectionView: SectionView = sectionNib.objectAtIndex(0) as? SectionView else {
            return UIView()
        }
        sectionView.setupData(self.eventsData.sections[section])
        return sectionView
    }
}


extension MyEventsInteractor: MyEventsActionsInteractor {
    
}