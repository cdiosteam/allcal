//
//  EventDetailsViewController.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 10/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import UIKit

class EventDetailsViewController: UIViewController {

    // MARK: -  Variables
    var event: Event?
    var users: [User]?
    
    // MARK: -  Outlets
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var shortTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var locationIconImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupData()
        setupDelegates()
        registerNib()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: -  Setup Methods
    func setupData() {
        self.shortTitleLabel.text = self.event?.shortTitle?.uppercaseString
        self.timeLabel.text = Utils().getEventIntervalTime(event)
        if let startDate = event?.startDate {
           self.dateLabel.text = Utils().getStringFromDateWithDateFormat(startDate, dateFormat: "MMMM ddT yyyy")
        }
        self.locationLabel.text = self.event?.location
        if self.event?.location == nil || self.event?.location == "" {
            self.locationIconImageView.hidden = true
        }
        self.descriptionLabel.text = self.event?.descriptionText
        if let url = self.event?.imageUrl {
            backgroundImageView.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        }
    }
    
    func setupNavigationBar() {
        navigationItem.title = event?.title ?? ""
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName :  UIColor(red:0.23, green:0.30, blue:0.33, alpha:1.0)]
        self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 0.91, green: 0.95, blue: 0.95, alpha: 1)
        
        let backButton = UIBarButtonItem(image: UIImage(named: "btn-back"), style: .Plain, target: self, action: #selector(EventDetailsViewController.backButtonAction))
        backButton.customView?.backgroundColor = UIColor.blueColor()
        navigationItem.leftBarButtonItem = backButton
    }
    
    // MARK: -  Actions
    @IBAction func shareButtonAction(sender: UIButton) {
    }
    
    func backButtonAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
}


extension EventDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func setupDelegates() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.emailTextField.delegate = self
    }
    
    func registerNib() {
        let userCustomNib = UINib(nibName: CollectionViewCell.UserCollectionViewCell, bundle: nil)
        collectionView.registerNib(userCustomNib, forCellWithReuseIdentifier: CellIdentifier.UserCellIdentifier)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellIdentifier.UserCellIdentifier, forIndexPath: indexPath) as? UserCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.setupData(users?[indexPath.row])
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return self.users?.count ?? 0
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(30,  30)
    }
    
    func collectionView(collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!, columnCountForSection section: Int) -> Int {
        return 1
    }

}

extension EventDetailsViewController: UITextFieldDelegate {
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.scrollView.setContentOffset(CGPoint(x: 0, y: textField.frame.origin.y + 100), animated: true)
    }
}
