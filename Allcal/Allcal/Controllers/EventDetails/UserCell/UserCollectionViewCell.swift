//
//  UserCollectionViewCell.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 11/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import UIKit

class UserCollectionViewCell: UICollectionViewCell {

    // MARK: -  Outlets
    @IBOutlet weak var imageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.roundImageView()
    }

    // MARK: -  Setup Methods
    func setupData(user: User?) {
        if let url = user?.imageUrl {
            imageView.kf_setImageWithURL(url, placeholderImage: TabBarImage.Account, optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        }
    }
}
