//
//  LoginViewController.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 02/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    // MARK: -  Outlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupDelegates()
        self.setupNavigationBar()
        self.setupTextFieldTags()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: -  Setup Methods
    func setupNavigationBar() {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setupDelegates() {
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
    }
    
    func setupTextFieldTags() {
        self.emailTextField.tag = LoginTextFieldTag.Email.rawValue
        self.passwordTextField.tag = LoginTextFieldTag.Password.rawValue
    }
    
    // MARK: -  Actions
    @IBAction func loginButtonAction(sender: UIButton) {
        self.view.endEditing(true)
        guard checkEmptyFieldsAndValidEmail() == false else {
            return
        }
        APIUserRequests.loginUserWithEmail(emailTextField.text!, password: passwordTextField.text!) { (token, username, imageProfileUrl, unitSetting, notificationSetting, vibrationSetting, error) in
            guard error == nil else {
                Utils.displayAlertController(self, title: "Login!", message: error!, cancelButtonTitle: "Ok")
                return
            }
            CurrentUser.sharedInstance.setCurrentUserDetails(token, email: self.emailTextField.text, username: username, imageProfileUrl: imageProfileUrl, unitsSetting: unitSetting, notificationSetting: notificationSetting, vibrationSetting: vibrationSetting)
            self.setupTabBarRoot()
        }
    }
    
    // MARK: -  Helper Methods
    func checkEmptyFieldsAndValidEmail() -> Bool {
        guard let email = emailTextField.text where email != "" else {
            Utils.displayAlertController(self, title: "Login", message: "Complete email field!", cancelButtonTitle: "Ok")
            return true
        }
        guard passwordTextField.text != "" else {
            Utils.displayAlertController(self, title: "Login", message: "Complete password field!", cancelButtonTitle: "Ok")
            return true
        }
        guard Utils.isValidEmail(email) == true else {
             Utils.displayAlertController(self, title: "Login", message: "Your email address is invalid. Please enter a valid address.", cancelButtonTitle: "Ok")
            return true
        }
        return false
    }
    
    func setupTabBarRoot() {
        dispatch_async(dispatch_get_main_queue()) {
            if let appDelegate  = UIApplication.sharedApplication().delegate as? AppDelegate {
                guard let window = appDelegate.window else {
                    return
                }
                AppController.launchTabBarInWindow(window)
            }
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField.tag {
        case LoginTextFieldTag.Email.rawValue:
            self.passwordTextField.becomeFirstResponder()
            break
        case LoginTextFieldTag.Password.rawValue:
            self.view.endEditing(true)
            break
        default:
            break
        }
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if (UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation) == false ) {
            self.scrollView.setContentOffset(CGPoint(x: 0, y: textField.frame.origin.y + 50), animated: true)
        }
        
    }
}


