//
//  NSDate+StartOfDay.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 10/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation

extension NSDate {
    var startOfDay: NSDate {
        return NSCalendar.currentCalendar().startOfDayForDate(self)
    }
    
    var endOfDay: NSDate? {
        let components = NSDateComponents()
        components.day = 1
        components.second = -1
        return NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: startOfDay, options: NSCalendarOptions())
    }
}

