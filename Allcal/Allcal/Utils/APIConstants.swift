//
//  APIConstants.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 15/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation


struct WebServicesUrl {
    static let ServerAddress = "asda",
    Login = "/login"
}

struct HTTPMethod {
    static let POST = "POST",
    GET = "GET"
}

