//
//  Constants.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 02/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation
import UIKit

//View Controllers
struct ViewControllerName {
    static let LoginViewController = "LoginViewController",
    AccountViewController = "AccountViewController",
    MyEventsTableViewController = "MyEventsTableViewController"
}

// CurrentUser Key
struct UserKey {
    static let Token = "token",
    Email = "email",
    Password = "password",
    Username = "username",
    ImageProfileUrl = "imageProfileUrl",
    UnitsSetting = "unitsSetting",
    NotificationSetting = "notificationSetting",
    VibrationSetting = "vibrationSetting"
}

struct TestUser {
    static let Email = "test@gmail.com",
    Password = "123456"
}

enum LoginTextFieldTag: Int {
    case Email
    case Password
}

// TabBar Images
struct TabBarImage {
    static let MyEvents = UIImage(named: "tabbar-myevents")!,
    Account = UIImage(named: "tabbar-account")!
    
}

struct TabBarTitle {
    static let MyEvents = "My Events",
    Account = "Account",
    Discovery = "Discovery",
    Activity = "Activity"
}

struct TableViewCell {
    static let UnitsTableViewCell = "UnitsTableViewCell",
    AccountCustomTableViewCell = "AccountCustomTableViewCell",
    EventTableViewCell = "EventTableViewCell",
    SectionView = "SectionView"
}

struct CollectionViewCell {
    static let UserCollectionViewCell = "UserCollectionViewCell"
}

struct CellIdentifier {
    static let UnitsCellIdentifier = "unitsCellIdentifier",
    AccountCustomIdentifier = "accountCustomIdentifier",
    MyEventsIdentifier = "myEventsIdentifier",
    UserCellIdentifier = "userCellIdentifier"
}

enum UnitsSegmentedControl: Int {
    case Imperial
    case Metric
}

struct UnitsDescriptionKey {
    static let Imperial = "Fahrenheit, 12H, MM/DD/YYYY",
    Metric = "Celsius, 24H, MM/DD/YYYY"
}

struct AccountCellType {
    static let Notification = "notification",
    Vibration = "vibration"
}

struct EventColor {
    static let Blue = UIColor(red:0.47, green:0.70, blue:0.84, alpha:1.0),
    Yellow = UIColor(red:0.98, green:0.80, blue:0.35, alpha:1.0),
    Pink = UIColor(red:0.80, green:0.61, blue:0.73, alpha:1.0)
}