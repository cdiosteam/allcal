//
//  Utils.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 03/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation
import UIKit


class Utils: NSObject {
    
    class func displayAlertController(target : UIViewController,title: String, message: String, cancelButtonTitle: String?) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancelAction : UIAlertAction = UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.Cancel) { (ACTION) -> Void in
            
        }
        alertController.addAction(cancelAction)
        target.presentViewController(alertController, animated: true, completion: nil)
    }
    
    class func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        if let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx) as NSPredicate? {
            return emailPredicate.evaluateWithObject(email)
        }
        return false
    }
    
    func getEventIntervalTime(event: Event?) -> String? {
        guard let event = event else {
            return ""
        }
        if let startDate = event.startDate, let endDate = event.endDate {
            let startDateOf = startDate.startOfDay
            let endDateOf = endDate.startOfDay
            
            if startDateOf.isEqualToDate(endDateOf) {
                if event.allDay == true {
                    return "All Day"
                } else {
                    let startTime = getStringFromDateWithDateFormat(startDate, dateFormat: "hh:mm a")
                    let endTime = getStringFromDateWithDateFormat(endDate, dateFormat: "hh:mm a")
                    return startTime + " - " + endTime
                }
            } else {
                if event.allDay == true {
                    return "All Day, " + getStringFromDateWithDateFormat(startDate, dateFormat: "MMM dd") + " - " + getStringFromDateWithDateFormat(endDate, dateFormat: "MMM dd")
                } else {
                     return  getStringFromDateWithDateFormat(startDate, dateFormat: "MMM dd, hh:mm a") + " - " + getStringFromDateWithDateFormat(endDate,  dateFormat: "MMM dd, hh:mm a")
                }
            }
        }
        return ""
    }
    
    func getStringFromDateWithDateFormat(date: NSDate, dateFormat: String) -> String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        return dateFormatter.stringFromDate(date)
    }
    
    class func createNSDateFromString(string: String) -> NSDate? {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        return formatter.dateFromString(string) // Returns "Jul 27, 2015, 12:29 PM" PST
    }
    
    
    class func createDummyDataForCalendar() -> MyEventsData {
        let dummyBackgroundImage = NSURL(string: "https://scontent.fotp3-1.fna.fbcdn.net/v/t1.0-9/1560525_697259253668400_784267978_n.jpg?oh=7cc1f6bc9010cda55d36a38cce527431&oe=5841AFBC")
        let dummyDescriptionText = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat v"
        
        
        let event0StartDate = Utils.createNSDateFromString("2016-09-03 19:29:50 +0000")
        let event0EndDate = Utils.createNSDateFromString("2016-09-13 19:29:50 +0000")
        
        let event1StartDate = Utils.createNSDateFromString("2016-09-04 01:29:50 +0000")
        let event1EndDate = Utils.createNSDateFromString("2016-09-14 19:29:50 +0000")
        
        let event1_1StartDate = Utils.createNSDateFromString("2016-09-13 19:29:50 +0000")
        let event1_1EndDate = Utils.createNSDateFromString("2016-09-14 19:29:50 +0000")
        
        let event2StartDate = Utils.createNSDateFromString("2016-09-15 19:29:50 +0000")
        let event2EndDate = Utils.createNSDateFromString("2016-09-16 20:29:50 +0000")
        
        let event3StartDate = Utils.createNSDateFromString("2016-09-17 19:29:50 +0000")
        let event3EndDate = Utils.createNSDateFromString("2016-09-18 20:29:50 +0000")
        
        let event4StartDate = Utils.createNSDateFromString("2016-09-18 19:29:50 +0000")
        let event4EndDate = Utils.createNSDateFromString("2016-09-19 20:29:50 +0000")
        
        let event0 = Event(id: 0, title: "Event 0", shortTitle: "Short Title 0", startDate: event0StartDate, endDate:  event0EndDate, allDay: false, color: EventColor.Blue, location: "location test", descriptionText: dummyDescriptionText, imageUrl: dummyBackgroundImage)
        
        let event1 = Event(id: 0, title: "Event 1", shortTitle: "Short Title 1", startDate: event1StartDate, endDate:  event1EndDate, allDay: false, color: EventColor.Blue, location: "location test", descriptionText: dummyDescriptionText, imageUrl: dummyBackgroundImage)
        
        let event1_1 = Event(id: 0, title: "Event 1.1", shortTitle: "Short title 1.1", startDate: event1_1StartDate, endDate:  event1_1EndDate, allDay: false, color: EventColor.Pink, location: nil, descriptionText: dummyDescriptionText, imageUrl: dummyBackgroundImage)
        
        let event2 = Event(id: 0, title: "Event 2", shortTitle: "Short title 2", startDate: event2StartDate, endDate:  event2EndDate, allDay: false, color: EventColor.Yellow, location: "location test 2", descriptionText: dummyDescriptionText, imageUrl: dummyBackgroundImage)
        
        let event3 = Event(id: 0, title: "Event 3", shortTitle: "Short title 3", startDate: event3StartDate, endDate:  event3EndDate, allDay: true, color: EventColor.Blue, location: nil, descriptionText: dummyDescriptionText, imageUrl: dummyBackgroundImage)
        
        let event4 = Event(id: 0, title: "Event 4", shortTitle: "Short title 4", startDate: event4StartDate, endDate:  event4EndDate, allDay: true, color: EventColor.Pink, location: "location test 4", descriptionText: dummyDescriptionText, imageUrl: dummyBackgroundImage)
        
        var eventsArray = [Event]()
        eventsArray.append(event0)
        eventsArray.append(event1)
        eventsArray.append(event1_1)
        eventsArray.append(event2)
        eventsArray.append(event3)
        eventsArray.append(event4)
        var usersArray = [User]()
        
        let user1 = User(name: "User1", email: "email1@gmail.com", imageUrl: NSURL(string: "https://scontent.fotp3-1.fna.fbcdn.net/v/t1.0-9/1560525_697259253668400_784267978_n.jpg?oh=7cc1f6bc9010cda55d36a38cce527431&oe=5841AFBC"))
        for _ in 0..<20 {
            usersArray.append(user1)
        }
        return MyEventsData(eventsArray: eventsArray, usersArray: usersArray)
    }
}