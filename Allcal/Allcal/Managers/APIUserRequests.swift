//
//  APIUserRequests.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 03/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation
import UIKit

class APIUserRequests: NSObject {
    static func loginUserWithEmail(email: String, password: String, completion:(token: String?, username: String?, imageProfileUrl: String?, unitSetting: Int?, notificationSetting: Bool?, vibrationSetting: Bool?, error: String?) -> Void) {
        if email == TestUser.Email && password == TestUser.Password {
            let token = String.random(10)
            completion(token: token, username: "username_test", imageProfileUrl: "https://scontent.fotp3-1.fna.fbcdn.net/v/t1.0-9/1560525_697259253668400_784267978_n.jpg?oh=7cc1f6bc9010cda55d36a38cce527431&oe=5841AFBC", unitSetting: 0, notificationSetting: false, vibrationSetting: false, error: nil)
        } else {
            completion(token: nil, username: nil,imageProfileUrl: nil, unitSetting: nil, notificationSetting: nil, vibrationSetting: nil, error: "Wrong email or password! For test try:\nEmail: test@gmail.com \nPassword: 123456")
        }
        
    }
    
    static func loginUser(email: String, completion: (String?) -> Void) {
        guard let url = NSURL(string: WebServicesUrl.Login) else {
            completion("Invalid URL")
            return
        }
        let body = ["email" : email, "adsasd": email]
        
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = HTTPMethod.POST
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("application/json; charset=uft-8", forHTTPHeaderField: "Content-Type")
        do {
            let jsonBody = try NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
            request.HTTPBody = jsonBody
            
            let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
                guard let _ = data, let responseHttp = response as? NSHTTPURLResponse where error == nil else {
                    completion("Request error")
                    return
                }
                
                guard responseHttp.statusCode == 200 else {
                    completion("Error")
                    return
                }
                
                if let token = responseHttp.allHeaderFields["Token"] as? String {
                    completion(token)
                } else {
                    completion(nil)
                }
                
            })
            task.resume()
        } catch {
            completion("JSON Error")
        }
    }
    
    
    
    
    
    
    
    static func testLogin(email: String, password: String, completion: (token: String?, error: String?) -> Void) {
        guard let url = NSURL(string: "aaa") else {
            completion(token: nil, error: "Invalid URL")
            return
        }
        let session = NSURLSession.sharedSession()
        let body = ["email": email, password: "password"]
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = HTTPMethod.POST
        do {
            let dataBody = try NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
            request.HTTPBody = dataBody
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            request.setValue("application/json; charset=uft-8", forHTTPHeaderField: "Content-Type")
            
            let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
                guard let responseHttp = response as? NSHTTPURLResponse where error == nil else {
                    completion(token: nil, error: "response error")
                    return
                }
                guard responseHttp.statusCode == 200 else {
                    completion(token: nil, error: "Status error")
                    return
                }
                
                if let token = responseHttp.allHeaderFields["Token"] as? String {
                    completion(token: token, error: nil)
                } else {
                    completion(token: nil, error: "Token error")
                }
            })
            task.resume()
        } catch {
            completion(token: nil, error: "JSON error")
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}