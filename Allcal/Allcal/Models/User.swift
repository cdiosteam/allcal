//
//  User.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 11/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation

class User: NSObject {
    var name: String?
    var email: String?
    var imageUrl: NSURL?
    
    init( name: String?,
     email: String?,
     imageUrl: NSURL?) {
        self.name = name
        self.email = email
        self.imageUrl = imageUrl
    }
}