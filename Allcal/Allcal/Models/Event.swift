//
//  Event.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 09/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation
import UIKit

class Event: NSObject {
    var id: Int?
    var title: String?
    var shortTitle: String?
    var startDate: NSDate?
    var endDate: NSDate?
    var allDay: Bool?
    var color: UIColor?
    var location: String?
    var descriptionText: String?
    var imageUrl: NSURL?
    
    init(  id: Int?,
     title: String?,
     shortTitle: String?,
     startDate: NSDate?,
     endDate: NSDate?,
     allDay: Bool?,
     color: UIColor?,
     location: String?,
     descriptionText: String?,
     imageUrl: NSURL?) {
        self.id = id
        self.title = title
        self.shortTitle = shortTitle
        self.startDate = startDate
        self.endDate = endDate
        self.allDay = allDay
        self.color = color
        self.location = location
        self.descriptionText = descriptionText
        self.imageUrl = imageUrl
    }
}

