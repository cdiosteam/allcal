//
//  CurrentUser.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 03/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation
import UIKit

class CurrentUser: NSObject {
    static let sharedInstance: CurrentUser = CurrentUser()
    
    // MARK: -  Variables
    var token: String? {
        get {
            return NSUserDefaults.standardUserDefaults().stringForKey(UserKey.Token)
        }
        set(token) {
            NSUserDefaults.standardUserDefaults().setValue(token, forKey: UserKey.Token)
        }
    }
    var email: String? {
        get {
            return NSUserDefaults.standardUserDefaults().stringForKey(UserKey.Email)
        }
        set(email) {
            NSUserDefaults.standardUserDefaults().setValue(email, forKey: UserKey.Email)
        }
    }
    var username: String? {
        get {
            return NSUserDefaults.standardUserDefaults().stringForKey(UserKey.Username)
        }
        set(username) {
            NSUserDefaults.standardUserDefaults().setValue(username, forKey: UserKey.Username)
        }
    }
    var imageProfileUrl: String? {
        get {
            return NSUserDefaults.standardUserDefaults().stringForKey(UserKey.ImageProfileUrl)
        }
        set(token) {
            NSUserDefaults.standardUserDefaults().setValue(token, forKey: UserKey.ImageProfileUrl)
        }
    }
    var unitsSetting: Int? {
        get {
            return NSUserDefaults.standardUserDefaults().integerForKey(UserKey.UnitsSetting)
        }
        set(unitsSetting) {
            NSUserDefaults.standardUserDefaults().setValue(unitsSetting, forKey: UserKey.UnitsSetting)
        }
    }
    var notificationSetting: Bool? {
        get {
            return NSUserDefaults.standardUserDefaults().boolForKey(UserKey.NotificationSetting)
        }
        set(notificationSetting) {
            NSUserDefaults.standardUserDefaults().setValue(notificationSetting, forKey: UserKey.NotificationSetting)
        }
    }
    var vibrationSetting: Bool? {
        get {
            return NSUserDefaults.standardUserDefaults().boolForKey(UserKey.VibrationSetting)
        }
        set(vibrationSetting) {
            NSUserDefaults.standardUserDefaults().setValue(vibrationSetting, forKey: UserKey.VibrationSetting)
        }
    }
    
    // MARK: -  Setter Methods
    func setCurrentUserDetails(
        token: String?,
        email: String?,
        username: String?,
        imageProfileUrl: String?,
        unitsSetting: Int?,
        notificationSetting: Bool?,
        vibrationSetting: Bool?) {
        self.token = token
        self.email = email
        self.username = username
        self.imageProfileUrl = imageProfileUrl
        self.unitsSetting = unitsSetting
        self.notificationSetting = notificationSetting
        self.vibrationSetting = vibrationSetting
    }
    
    func logoutCurrentUser() {
        self.token = nil
        self.email = nil
        self.username = nil
        self.imageProfileUrl = nil
        self.unitsSetting = nil
        self.notificationSetting = nil
        self.vibrationSetting = nil
    }
    
    func isLoggedIn() -> Bool {
        guard token != nil else {
            return false
        }
        return true
    }
    
    override var description: String {
        return "Token = \(self.token), Email = \(self.email), Username = \(self.username), PhotoUrl = \(self.imageProfileUrl), UnitsSetting = \(self.unitsSetting), NotificationSetting = \(self.notificationSetting), VibrationSetting = \(self.vibrationSetting)"
    }
}
