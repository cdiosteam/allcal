//
//  AppController.swift
//  Allcal
//
//  Created by Cristian Crasneanu on 02/09/16.
//  Copyright © 2016 Cristian Crasneanu. All rights reserved.
//

import Foundation
import UIKit

class AppController: NSObject {

    class func launchLoginInWindow(window: UIWindow) {
        window.rootViewController = self.createAndReturnRoot()
        window.makeKeyAndVisible()
    }
    
    class func launchTabBarInWindow(window: UIWindow) {
        window.rootViewController = self.createTabBar()
        window.makeKeyAndVisible()
    }
    
    class func createAndReturnRoot() -> UIViewController {
        let loginViewController = LoginViewController(nibName: ViewControllerName.LoginViewController, bundle: nil)
        let navigationController = UINavigationController(rootViewController: loginViewController)
        return navigationController
    }
    
    class func setupLoginRegisterRoot() {
        if let appDelegate  = UIApplication.sharedApplication().delegate as? AppDelegate {
            UIView.transitionWithView(appDelegate.window!, duration: 0.5, options: UIViewAnimationOptions.TransitionFlipFromLeft, animations: { () -> Void in
                let loginRegisterViewController = LoginViewController(nibName: ViewControllerName.LoginViewController, bundle: nil)
                appDelegate.window?.rootViewController = UINavigationController(rootViewController: loginRegisterViewController)
                }, completion: nil)
        }
    }
    
    class func createTabBar() -> UITabBarController {
        let tabBarController = UITabBarController()
        
        let eventsData =  Utils.createDummyDataForCalendar()
        let myEventsInteractor = MyEventsInteractor(Data: eventsData)
        let myEventsTableViewController = MyEventsTableViewController.instantiateInstanceViewController(myEventsInteractor)
        
        let accountViewController = AccountViewController(nibName: ViewControllerName.AccountViewController, bundle: nil)

        myEventsTableViewController.tabBarItem = UITabBarItem(title: TabBarTitle.MyEvents, image: TabBarImage.MyEvents, selectedImage: TabBarImage.MyEvents)
        accountViewController.tabBarItem = UITabBarItem(title: TabBarTitle.Account, image: TabBarImage.Account, selectedImage: TabBarImage.Account)
        
        let myEventsNavigationController = UINavigationController(rootViewController: myEventsTableViewController)
        let accountNavigationController = UINavigationController(rootViewController: accountViewController)
        
        let controllers = [myEventsNavigationController, accountNavigationController]
        tabBarController.viewControllers = controllers
        tabBarController.tabBar.tintColor = UIColor(red:0.29, green:0.75, blue:0.86, alpha:1.0)
        return tabBarController
    }
}